/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable no-var */
import { Selector } from "testcafe";
import { RequestMock } from "testcafe";

import * as listingTodo from "../cypress/fixtures/todo.json";
import * as locators from "../cypress/locators/todo.json";

const urlPlpPage = "https://todotestct.netlify.app/";

var seedData = RequestMock()
  .onRequestTo(/\/api\/todos/)
  .respond(listingTodo);

fixture`todo listing page`.page(urlPlpPage);

test.requestHooks(seedData)("should load todolist", async (t) => {
  await t.expect(Selector(locators.todoList).visible).ok();
  const osCount = await Selector("strong").innerText;
  await t.expect(osCount).eql("3");
});
