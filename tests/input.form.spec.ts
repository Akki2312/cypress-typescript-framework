/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable no-var */
import { Selector } from "testcafe";
import { RequestMock } from "testcafe";

import * as listingTodo from "../cypress/fixtures/todo.json";
import * as locators from "../cypress/locators/todo.json";

const urlPlpPage = "https://todotestct.netlify.app/";

var seedData = RequestMock()
  .onRequestTo(/\/api\/todos/)
  .respond(listingTodo);

fixture`Input form`.page(urlPlpPage);

test.requestHooks(seedData)("focuses input on load", async (t) => {
  const element = await Selector(locators.newTodo);
  const state = await element();
  await t.expect(state.focused).eql(true);
});

test.requestHooks(seedData)("accepts input", async (t) => {
  const typedText = "Buy Wine";
  const element = await Selector(locators.newTodo);
  await t.typeText(element, typedText);
  await t.expect(element.value).eql(typedText);
});
