// in cypress/support/index.d.ts
// load type definitions that come with Cypress module
/// <reference types="cypress" />

declare namespace Cypress {
  interface Chainable {
    /**
     * Custom command to get stub list of tasks
     * @example cy.seedAndVisit()
     */
    seedAndVisit(object): void;
    /**
     * Custom command to add item in to do list
     * @example cy.addTodoItem'("study")
     */
    addTodoItem(task: string): void;
  }
}
