/* eslint-disable no-undef */
// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//

Cypress.Commands.add("addTodoItem", (task) => {
  cy.get('[data-reactid=".0.0.1"]').type(task).type("{enter}");
});

Cypress.Commands.add("seedAndVisit", (seedData = "fixture:todo") => {
  cy.server();
  cy.route("GET", "/api/todos", seedData);
  cy.visit("/");
});
