import * as locators from "../locators/todo.json";

describe("App failure errors", () => {
  it("Displays an error on failure", () => {
    cy.intercept({
      url: "/api/todos",
      method: "GET",
      status: 500,
      response: {},
    });
    cy.visit("/");

    cy.get(locators.todolist).should("not.exist");

    cy.get(locators.error).should("be.visible");
  });
});
