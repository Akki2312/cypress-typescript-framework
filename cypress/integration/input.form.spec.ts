import * as locators from "../locators/todo.json";
describe("Input form", () => {
  beforeEach(() => {
    cy.seedAndVisit();
  });

  it("focuses input on load", () => {
    cy.focused().should("have.class", "new-todo");
  });

  it("accepts input", () => {
    const typedText = "Buy Wine";
    cy.get(locators.newTodo).type(typedText).should("have.value", typedText);
  });
});
