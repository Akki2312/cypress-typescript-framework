import * as locators from "../locators/todo.json";

describe("Single footer", () => {
  it("displays a singular todo in count", () => {
    cy.seedAndVisit([{ id: 1, name: "Buy Cola", isComplete: false }]);
    cy.get(locators.count).should("contain", "1 todo left");
  });
});
