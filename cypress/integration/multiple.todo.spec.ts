import * as locators from "../locators/todo.json";

describe("Footer", () => {
  beforeEach(() => {
    cy.seedAndVisit();
  });

  it("displays plural todos in count", () => {
    cy.get(locators.count).should("contain", "3 todos left");
  });

  it("Handles filter links", () => {
    const filters = [
      { link: "Active", expectedLength: 3 },
      { link: "Completed", expectedLength: 1 },
      { link: "All", expectedLength: 4 },
    ];
    cy.wrap(filters).each((filter) => {
      cy.contains(filter.link).click();

      cy.get(locators.todoList).should("have.length", filter.expectedLength);
    });
  });
});
