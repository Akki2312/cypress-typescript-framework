import * as locators from "../locators/todo.json";

describe("App initialization", () => {
  it("Loads todos on page load", () => {
    cy.seedAndVisit();
    cy.get(locators.todoList).should("have.length", 4);
  });
});
