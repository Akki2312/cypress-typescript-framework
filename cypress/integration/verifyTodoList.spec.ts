import * as locators from "../locators/todo.json";

describe("verfiy to do  list", () => {
  beforeEach(() => {
    cy.seedAndVisit();
  });
  it("shoud show uncompleted task count", () => {
    cy.visit("/");
    cy.get(locators.count).should("contain", 3);
  });
});
